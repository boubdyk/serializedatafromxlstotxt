package com.cacka.fromXls;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedList;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

public class GetDataFromXlsByCells {

	LinkedList<User> listUser = new LinkedList<User>();
	User user;
	void getData(){
		try {
			InputStream in = new FileInputStream("D:\\data.xls");
			HSSFWorkbook wb = new HSSFWorkbook(in);			
			Sheet sheet = wb.getSheetAt(0); 
			Iterator<Row> iterator = sheet.iterator();
			while (iterator.hasNext()) {
				Row row = iterator.next();
				Iterator<Cell> cells = row.iterator();
				user = new User();
				while (cells.hasNext()) {
					Cell cell = cells.next();
					int celltype = cell.getCellType();
					switch (celltype) {
						case Cell.CELL_TYPE_STRING:
							user.setName(cell.getStringCellValue());
							break;
						case Cell.CELL_TYPE_NUMERIC:
							user.setId((int) cell.getNumericCellValue());
							break;
					}
				}
				listUser.add(user);				
			}
		} catch (FileNotFoundException e) {
			System.out.println("Error with opening file.");
		} catch (IOException e) {
			System.out.println("Error with creating HSSFWorkbook");
		}
	}
}
