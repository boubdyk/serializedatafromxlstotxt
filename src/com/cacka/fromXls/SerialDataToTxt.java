package com.cacka.fromXls;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerialDataToTxt {
	
	void setData(){
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("D:\\dataXLS.txt"));	
			GetDataFromXlsByCells data = new GetDataFromXlsByCells();
			data.getData();	
			oos.writeObject(data.listUser);
			oos.flush();
			oos.close();			
		} catch (FileNotFoundException e) {
			System.out.println("Error with creating output file.");
		} catch (IOException e) {
			System.out.println("Error with creating OOS.");
		}
	}
	
	public static void main(String[] args) {
		SerialDataToTxt txt = new SerialDataToTxt();
		txt.setData();
	}
}
