package com.cacka.fromXls;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.LinkedList;

public class ReadDataFromTxt {
	
	LinkedList<User> users = new LinkedList<User>(); 

	void readData() {
		try {
			ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream("D:\\dataXLS.txt")));			
			users = (LinkedList<User>)ois.readObject();			
		} catch (FileNotFoundException e) {
			System.out.println("File not found.");
		} catch (IOException e) {
			System.out.println("IOException");
		} catch (ClassNotFoundException e) {
			System.out.println("Class not found");
		}
	}
	
	public static void main(String[] args) {
		ReadDataFromTxt rd = new ReadDataFromTxt();
		rd.readData();
		for (int i = 0; i < rd.users.size(); i++){
			System.out.println(rd.users.get(i).getName() + " = " + rd.users.get(i).getId());
		}
	}
}
