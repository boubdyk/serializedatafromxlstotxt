package com.cacka.strings;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ReadFromTxt {

	StringBuffer readData() {
		try {
			// Reading txt file
			BufferedReader in = new BufferedReader(new FileReader("D:\\Javatmpfiles\\in.txt"));			
			int c;
			StringBuffer buff = new StringBuffer();
			while ((c = in.read()) != -1) {
				// while not eof then append buff
				if (c != '\n'){
					buff.append((char)c);
				}				
			}			
			return buff;			
		} catch (FileNotFoundException e) {
			System.out.println("File not found.");
			return null;
		} catch (IOException e) {
			System.out.println("Could not read data from file.");
			return null;
		}		
	}
}
