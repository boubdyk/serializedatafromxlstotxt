package com.cacka.strings;

public class Filtering {

	StringBuffer filter(StringBuffer buff) {
		String[] text = buff.toString().split(" ");
		StringBuffer totalText = new StringBuffer();
		for (String str: text) {
			if (str.indexOf("ko") != -1) {
				totalText.append(str + " ");
			}
		}
		return totalText;
	}
}
